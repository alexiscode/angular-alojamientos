export default function AlojamientosService($resource) {
  "ngInject";

  return $resource('/api/search_results?client_id=3092nxybyb0otqw18e8nh5nty&location=:location&_limit=:limit&user_lat=:latitude&user_lng=:longitude', {
    latitude: '@latitude',
    longitude: '@longitude',
    location: '@location',
    limit: '@limit'
  }, {
    query: {
      method: 'GET',
      isArray: false
    },
    getAlojamientoById: {
      method: 'GET',
      params: {
        id: '@id'
      },
      url: '/api/listings/:id?client_id=3092nxybyb0otqw18e8nh5nty&_format=v1_legacy_for_p3'
    }
  });
}
