import template from './alojamientos-list.html';
import controller from './alojamientos-list.controller';

const AlojamientosListComponent = {
  template,
  controller
};

export default AlojamientosListComponent;
