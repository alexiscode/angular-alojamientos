class AlojamientosListController {

  constructor(AlojamientosService, $log) {
    "ngInject";
    this.$log = $log;
    this.AlojamientosService = AlojamientosService;
    this.alojamientos = [];
    this.getAlojamientos();
  }

  getAlojamientos() {
    this.AlojamientosService.query({
      location: 'los angeles',
      limit: 30
    }).$promise.then(alojamientos => {
      this.alojamientos = alojamientos.search_results;
    });
  }
}

export default AlojamientosListController;
