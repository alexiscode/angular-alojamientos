import angular from 'angular';
import 'angular-loading-bar';
import headerComponent from './app-header.component';
import sidebarComponent from './app-sidebar.component';

const CommonModule = angular.module('app.common', ['angular-loading-bar'])
  .component('appHeader', headerComponent)
  .component('appSidebar', sidebarComponent)
  .run(($transitions, cfpLoadingBar) => {
    'ngInject';
    $transitions.onStart({}, cfpLoadingBar.start);
    $transitions.onSuccess({}, cfpLoadingBar.complete);
  })
  .name;

export default CommonModule;
