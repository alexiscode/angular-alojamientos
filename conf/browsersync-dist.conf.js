const conf = require('./gulp.conf');
const url = require('url');
const proxy = require('proxy-middleware');

module.exports = function () {
  const proxyOptions = url.parse('https://api.airbnb.com/v2');
  proxyOptions.route = '/api';
  return {
    server: {
      baseDir: [
        conf.paths.dist
      ],
      middleware: [proxy(proxyOptions)]
    },
    open: false
  };
};
