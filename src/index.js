import angular from 'angular';

import 'angular-ui-router';
import 'angular-resource';
import 'angular-storage';
import 'angular-google-maps';
import 'angular-simple-logger';

import routesConfig from './routes';
import Common from './app/common/';
import Components from './app/components/';
import AppComponent from './app/common/app.component';

import 'bootstrap/dist/css/bootstrap.css';
import './index.scss';

angular
  .module('app', [
    Common,
    Components,
    'ui.router',
    'angular-storage',
    'ngResource',
    'nemLogging',
    'uiGmapgoogle-maps'
  ])
  .config(routesConfig)
  .config(uiGmapGoogleMapApiProvider => {
    'ngInject';
    uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyBm0UPS0Gi1ofkfz3J-XjOG4cV9-xy0PVo',
      // v: '3.20',
      libraries: 'weather,geometry,visualization'
    });
  })
  .component('app', AppComponent);
