import angular from 'angular';
import AlojamientosModule from './alojamientos';

const ComponentsModule = angular.module('app.components', [
  AlojamientosModule
]).name;

export default ComponentsModule;
