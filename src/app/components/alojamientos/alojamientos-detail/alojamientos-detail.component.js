import template from './alojamientos-detail.html';
import controller from './alojamientos-detail.controller';
import './alojamientos-detail.css';

const AlojamientoDetailComponent = {
  template,
  controller,
  bindings: {
    alojamiento: '<'
  }
};

export default AlojamientoDetailComponent;
