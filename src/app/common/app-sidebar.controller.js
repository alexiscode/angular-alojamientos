class AppSidebarController {
  constructor() {
    this.menuItems = [{
      label: 'Listado',
      icon: 'glyphicon glyphicon-home',
      state: 'alojamientos'
    }, {
      label: 'Mapas',
      icon: 'glyphicon glyphicon-map-marker',
      state: 'maps'
    }, {
      label: 'Favoritos',
      icon: 'glyphicon glyphicon-star',
      state: 'favorites'
    }];

    this.activeMenu = this.menuItems[0];
  }

  setActive(menuItem) {
    this.activeMenu = menuItem;
  }

}

export default AppSidebarController;
