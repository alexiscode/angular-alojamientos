import angular from 'angular';
import 'angular-ui-router';

import Alojamiento from './alojamiento/alojamiento.component';
import AlojamientosListComponent from './alojamientos-list/alojamientos-list.component';
import AlojamientosDetailComponent from './alojamientos-detail/alojamientos-detail.component';
import AlojamientosMapsComponent from './alojamientos-maps/alojamientos-maps.component';
import AlojamientosService from './alojamientos.service';

const AlojamientosModule = angular
  .module('alojamientos', ['ui.router'])
  .factory('AlojamientosService', AlojamientosService)
  .component('alojamiento', Alojamiento)
  .component('alojamientos', AlojamientosListComponent)
  .component('alojamientosMaps', AlojamientosMapsComponent)
  .component('alojamientosDetail', AlojamientosDetailComponent)
  .config($stateProvider => {
    "ngInject";

    $stateProvider
      .state('alojamientos', {
        url: '/',
        component: 'alojamientos'
      });

    $stateProvider
      .state('maps', {
        url: '/mapas',
        component: 'alojamientosMaps',
        resolve: {
          results: AlojamientosService => {
            "ngInject";
            return AlojamientosService.query({
              location: 'los angeles',
              limit: 30
            }).$promise;
          }
        }
      });

    $stateProvider
      .state('alojamiento', {
        url: '/alojamiento/:id',
        component: 'alojamientosDetail',
        resolve: {
          alojamiento: ($transition$, AlojamientosService) => {
            "ngInject";
            const key = $transition$.params().id;
            return AlojamientosService.getAlojamientoById({
              id: key
            }).$promise;
          }
        }
      });
  })
  .name;

export default AlojamientosModule;
