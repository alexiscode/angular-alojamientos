class AlojamientosDetailController {
  constructor(uiGmapGoogleMapApi, $log) {
    "ngInject";

    $log.info(this.alojamiento.listing);
    this.latitude = this.alojamiento.listing.lat;
    this.longitude = this.alojamiento.listing.lng;

    uiGmapGoogleMapApi.then(() => {
      this.options = {
        scrollwheel: false
      };
      this.map = {
        center: {
          latitude: this.latitude,
          longitude: this.longitude
        },
        zoom: 14
      };

      this.marker = {
        id: 0,
        coords: {
          latitude: this.latitude,
          longitude: this.longitude
        }
      };
    });
  }
}

export default AlojamientosDetailController;
