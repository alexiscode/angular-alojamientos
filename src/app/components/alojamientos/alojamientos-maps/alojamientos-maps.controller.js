class AlojamientosMapsController {
  constructor(uiGmapGoogleMapApi, $log) {
    "ngInject";
    this.$log = $log;
    this.markers = [];

    uiGmapGoogleMapApi.then(() => {
      angular.forEach(this.results.search_results, alojamiento => {
        const price = alojamiento.pricing_quote.localized_nightly_price;
        this.markers.push({
          id: alojamiento.listing.id,
          latitude: alojamiento.listing.lat,
          longitude: alojamiento.listing.lng,
          title: price,
          show: true,
          markerOptions: {
            visible: false
          }
        });
      });

      this.options = {
        scrollwheel: false
      };
      this.map = {
        center: {
          latitude: '34.052234',
          longitude: '-118.243685'
        },
        zoom: 10
      };
    });
  }
}
export default AlojamientosMapsController;
