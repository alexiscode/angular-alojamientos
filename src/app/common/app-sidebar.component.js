import controller from './app-sidebar.controller';
import './app-sidebar.css';

const sidebarComponent = {
  template: require('./app-sidebar.html'),
  controller
};

export default sidebarComponent;
