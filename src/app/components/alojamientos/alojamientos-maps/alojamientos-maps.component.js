import template from './alojamientos-maps.html';
import controller from './alojamientos-maps.controller';
import './alojamientos-maps.css';

const AlojamientosMapsComponent = {
  template,
  controller,
  bindings: {
    results: '<'
  }
};

export default AlojamientosMapsComponent;
