import template from './alojamiento.html';
import './alojamiento.scss';

const AlojamientoComponent = {
  template,
  bindings: {
    data: '<',
    addFavorite: '&'
  }
};

export default AlojamientoComponent;
